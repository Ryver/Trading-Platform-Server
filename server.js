const express = require('express');
const Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://piotrgorski:1234@localhost:5432/TradingAppDB');
const bodyParser = require('body-parser');

const userRoutes = require('./routes/userRoutes');

//Set up express app
const app = express();

//Implement bodyParser
app.use(bodyParser.json());

//Implement user routes
app.use('/api/user', userRoutes);

//Listen for requests
app.listen(process.env.port||3020, () => {
  console.log('Listening for request...');
});

//Test db connection
sequelize.authenticate()
.then(() => {
  console.log('Connection has been established successfully.')
})
.catch((err) => {
  console.log('Unable to connet co the database:', err)
})