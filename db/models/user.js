const Sequelize = require('sequelize');
const sequelize = new Sequelize('postgres://piotrgorski:1234@localhost:5432/TradingAppDB')


const User = sequelize.define('user', {
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  myFxBook: {
    type: Sequelize.STRING
  },
  ownWebsite: {
    type: Sequelize.STRING
  },
  expirience: {
    type: Sequelize.STRING
  },
  token: {
    type: Sequelize.STRING
  }
});

module.exports = User;