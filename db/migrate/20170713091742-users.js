'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.addColumn(
      'users',
      'firstName',
      Sequelize.STRING
    )
    queryInterface.addColumn(
      'users',
      'lastName',
      Sequelize.STRING
    )
    queryInterface.addColumn(
      'users',
      'myFxBook',
      Sequelize.STRING
    )
    queryInterface.addColumn(
      'users',
      'ownWebsite',
      Sequelize.STRING
    )
    queryInterface.addColumn(
      'users',
      'expirience',
      Sequelize.STRING
    )
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
  }
};
