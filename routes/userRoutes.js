const express = require('express');
const expressValidator = require('express-validator');
const router = express.Router();
const randToken = require('rand-token');

const User = require('../db/models/user');

//Implement expressValidator
router.use(expressValidator());

//Sign in request
router.post('/signIn', (req, res) => {
  User.findOne({ where: { email: req.body.email } })
  .then((user) => {
    if(user) {
      if(user.password == req.body.password) {
        res.send({ 
          id: user.id,
          email: user.email,
          username: user.username,
          token: user.token,
          msg: 'Success'
        })
      } else {
        res.send({ msg: 'Login failed, password does not match.' })
      }
    } else {
      res.send({ msg: 'User with this email does not exist.' })
    }
  })
  .catch((err) => console.log(err))
});

//Sign up request
router.post('/signUp', (req, res) => {

  req.checkBody('email', 'Email field is empty or has badly format').notEmpty().isEmail();
  req.checkBody('password', 'Password must has minimum 6 chars').isLength(6, 32);

  const errors = req.validationErrors();

  if(errors) {
    res.send(errors)
  } else {
    User.create({
      email: req.body.email,
      password: req.body.password,
      username: req.body.username,
      token: randToken.generate(32)
    })
    .then(() => {
      res.send({ msg: 'Success' })
    })
    .catch((err) => console.log(err))
  }
});

//Change password request
router.put('/:id/changePassword', (req, res) => {
  User.findById(req.params.id)
  .then((user) => {
    if(user) {
      if(user.password == req.body.oldPassword) {
        user.updateAttributes({
          password: req.body.password
        })
        .then(() => {
          res.send({ msg: 'Password changed.' })
        })
        .catch((err) => {
          console.log(err)
        })
      } else {
        res.send({ msg: 'Old password does not match.' })
      }
    } else {
      res.send({ msg: 'User with this id does not exist.' })
    }
  })
});

//Edit profile
router.put('/:id/editProfile', (req, res) => {
  User.findById(req.params.id)
  .then((user) => {
    if(user) {
      user.updateAttributes({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        myFxBook: req.body.myFxBook,
        ownWebsite: req.body.ownWebsite,
        expirience: req.body.expirience
      })
    } else {
      console.log('Error! This user does not exist.');
    }
  })
})

module.exports = router;